
import dynamic from 'next/dynamic'
const FormLogin = dynamic(() => import('@/app/components/FormLogin'));

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
        <FormLogin />
    </main>
  )
}
