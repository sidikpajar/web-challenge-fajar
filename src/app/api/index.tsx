/* eslint-disable no-undef */
import axios from "axios";

const api = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_HOSTNAME,
  headers: {
    "x-api-key": "YmVhdXRpaGF1bDEyMzpVU0VSTkFNRV9CQVNJQ19BVVRI",
    "Content-Type": "application/json",
  },
  /* other custom settings */
  validateStatus: () => true,
});

export {api};
